import axios from "axios";
import * as CONS from "./constants";
//import {getData} from '../models/token';

//const token = getData("token");

export async function pokemon() {
  try {
    const res = await axios.get(CONS.pokemon);
    return res;
  }
  catch(err) {
    return err;
  }
}

export async function pokemonData(id) {
  try {
    let pokemon = CONS.pokemon + id;
    const res = await axios.get(pokemon);
    return res;
  }
  catch(err) {
    return err;
  }
}

/*
  export async function setLogin(data) {
    //console.log("API -->", data);
    try {
      const res = await axios.post(CONS.login,data);
      return res.data;
    }
    catch(err) {
      return err;
    }
  }

  export async function animeList() {
    try {
      const jsn = {
        'headers':{'auth': token}
      };

      const res = await axios.get(CONS.list,jsn);

      return res.data;
    }
    catch(err) {
      return err;
    }
  }

  export async function animeView(idAnime) {
    try {
      const jsn = {
        'headers':{'auth': token}
      };
      
      const res = await axios.get(CONS.view + idAnime,jsn);
      return res.data;
    }
    catch(err) {
      return err;
    }
  }

  export async function registerUser(userData) {
    try {
      const res = await axios.post(CONS.register,userData);
      return res.data;
    }
    catch(err) {
      return err;
    }
  }
*/
