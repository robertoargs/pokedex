const URL = "https://pokeapi.co/api/v2";

//ENDPOINTS
export const ability = `${URL}/ability/`;
export const berry = `${URL}/berry/`;
export const berryFirmness = `${URL}/berry-firmness/`;
export const berryFlavor = `${URL}/berry-flavor/`;
export const characteristic = `${URL}/characteristic/`;
export const contestEffect = `${URL}/contest-effect/`;
export const contesType = `${URL}/contest-type/`;
export const eggGroup = `${URL}/egg-group/`;
export const encounterCondition = `${URL}/encounter-condition/`;
export const encounterConditionValue = `${URL}/encounter-condition-value/`;
export const encounterMethod = `${URL}/encounter-method/`;
export const evolutionChain = `${URL}/evolution-chain/`;
export const evolutionTrigger = `${URL}/evolution-trigger/`;
export const gender = `${URL}/gender/`;
export const generation = `${URL}/generation/`;
export const growthRate = `${URL}/growth-rate/`;
export const item = `${URL}/item/`;
export const itemAttribute = `${URL}/item-attribute/`;
export const itemCategory = `${URL}/item-category/`;
export const itemFlingEffect = `${URL}/item-fling-effect/`;
export const itemPocket = `${URL}/item-pocket/`;
export const language = `${URL}/language/`;
export const location = `${URL}/location/`;
export const locationArea = `${URL}/location-area/`;
export const machine = `${URL}/machine/`;
export const move = `${URL}/move/`;
export const moveAilment = `${URL}/move-ailment/`;
export const moveBattleStyle = `${URL}/move-battle-style/`;
export const moveCategory = `${URL}/move-category/`;
export const moveDamageClass = `${URL}/move-damage-class/`;
export const moveLearnMethod = `${URL}/move-learn-method/`;
export const moveTarget = `${URL}/move-target/`;
export const nature = `${URL}/nature/`;
export const palParkArea = `${URL}/pal-park-area/`;
export const pokeathlonStat = `${URL}/pokeathlon-stat/`;
export const pokedex = `${URL}/pokedex/`;
export const pokemon = `${URL}/pokemon/`;
export const pokemonColor = `${URL}/pokemon-color/`;
export const pokemonForm = `${URL}/pokemon-form/`;
export const pokemonHabitat = `${URL}/pokemon-habitat/`;
export const pokemonShape = `${URL}/pokemon-shape/`;
export const pokemonSpecies = `${URL}/pokemon-species/`;
export const region = `${URL}/region/`;
export const stat = `${URL}/stat/`;
export const superContestEffect = `${URL}/super-contest-effect/`;
export const type = `${URL}/type/`;
export const version = `${URL}/version/`;
export const versionGroup = `${URL}/version-group/`;

