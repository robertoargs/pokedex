import React, { Component } from 'react';
import { Card, CardHeader, CardImg, CardBody, Button, Col } from 'reactstrap';
import { Link } from 'react-router-dom';

import './style/ItemCard.css';

class ItemCard extends Component {
  render() { 
    const {name,id} = this.props.data;
    const {front_default} = this.props.data.sprites;
    return (  
      <Col sm="3">
        <Card className="mb-5" body color="primary" outline>
          <CardHeader className="cardHeader"># {id}: {name}</CardHeader>
          <CardImg top src={front_default} alt={name} className="pkmImage"/>
          <CardBody>
            {/*<CardTitle>{this.props}</CardTitle>*/}
            {/*<CardSubtitle>{this.props.data.id}</CardSubtitle>*/}
            {/*<CardText>Texto de Prueba</CardText>*/}
            {/*<CardFooter className="text-muted">Pie de carta</CardFooter>*/}
            
              <div className="buttonCont">
                <Link to={`/pokedex/${id}`}>
                  <Button className="viewButton">View Info</Button>
                </Link>
                <Button className="viewButton2" onClick={() => this.props.onModal(this.props.data)}>Shiny</Button>
              </div>
           
          </CardBody>
        </Card>
      </Col>
    );
  }
}
 
export default ItemCard;