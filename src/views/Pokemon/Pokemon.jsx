import React, { Component } from 'react';

import NavBar from '../../components/NavBar/NavBar';
import Footer from '../../components/Footer/Footer';
import ItemCard from './ItemCard';
import { Row } from 'reactstrap';
import ModalWindow from '../../components/Modal/ModalWindow';

import { pokemonData } from '../../api/api';
import ModalConfig from '../../utils/modalConfig';

//import { pokemon } from '../../api/api';
//import attributeExtract from '../../utils/extract-endpoints';

class Pokemon extends Component {
  state = { 
    pokemon:[
      //{'name':'Bulbasaur', 'sprites':{'front_default':"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png"}},
      //{'name':'Charmander', 'sprites':{'front_default':"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/4.png"}}
    ],
    offset:1,
    limit:25,
    modal:{
      isOpen:false,
      data:{'name':'Charmander',"id":"4", 'sprites':{'front_default':"", "front_shiny":"#", "back_default":"#", "back_shiny":"#"}},
      backdrop:false,
      keyboard:false
    }
  }

  componentDidMount() {
    this.pokemonData();
  }

  async pokemonData() {
    const {offset, limit} = this.state;
    for (let i = offset; i<=limit; i++) {
      let res = await pokemonData(i);
      this.state.pokemon.push(res.data)
    }
    this.setState({pokemon:this.state.pokemon},() => console.log(this.state))
    //console.log(this.state)
  }

  handleMore = () => {
    let newOffset = this.state.limit + 1; 
    let newLimit = this.state.limit + 25; 
    this.setState({offset:newOffset, limit:newLimit}, () => this.pokemonData());
  }

  handleLess = () => {
    alert("Cargar menos elementos")
  }

  handleModal = (data) => {
    let modalData = ModalConfig(true,data);
    this.setState({modal:modalData.modal}, () => console.log(this.state))
  }

  handleClose = () => {
    let modalData = ModalConfig(false,{});
    this.setState({modal:modalData.modal})
  }
  
  render() { 
    return ( 
      <>
        <ModalWindow open={this.state.modal} onClose={this.handleClose}/>
        <NavBar onMore={this.handleMore} onLess={this.handleLess}/>
        <div className="container" style={{marginTop:'7em'}}>
          <Row>
            {this.state.pokemon.map(pokemon => (<ItemCard key={pokemon.name} data={pokemon} onModal={this.handleModal}/>))}
          </Row>
        </div>
        <Footer/>
      </>
    );
  }
}
 
export default Pokemon;
