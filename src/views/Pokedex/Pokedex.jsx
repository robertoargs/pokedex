import React, { Component } from 'react';

import NavPokemon from '../../components/NavPokemon/NavPokemon';
import Footer from '../../components/Footer/Footer';
import Content from './Content';

//import { Container, Badge, Row, Col, CardImg } from 'reactstrap';

import { pokemonData } from '../../api/api';

import './style/Pokedex.css';

class Pokedex extends Component {

  constructor(props) {
    super(props);
    this.state = { 
      pokemon:{
      }
    }
  }

  componentDidMount() {
    this.pokemonData();
  }
  
  async pokemonData() {
    let id = this.props.match.params.id;
    const res = await pokemonData(id);
    this.setState({pokemon:res.data},() => console.log(this.state)); 
  }

  render() { 
    //console.log(this.state.pokemon.sprites)
    //const {id, name} = this.state.pokemon;
    return (
      <>
        <NavPokemon/>
          <Content data={this.state.pokemon}/>
        <Footer/>
      </>
    );
  }
}
 
export default Pokedex;