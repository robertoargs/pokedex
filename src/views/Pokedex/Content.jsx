import React, { Component } from 'react';

import { Container, Badge, Row, Col, CardImg } from 'reactstrap';

class Content extends Component {
  render() { 
    const {name, id} = this.props.data;
    //const {front_default} = this.props.data.sprites.front_default;

    return ( 
    <>
      <Container style={{marginTop:'10%', marginBottom:'40%'}}>
        <h1 className="title"> Data about Pokemon # {id}: {name}</h1>

        <Row>
          <Col sm="3">
            <CardImg top src={'hello'} alt={name}/>
          </Col>
          <Col sm="3">
            <img src={'hello'} alt={name}/>
          </Col>

          <Col sm="3">
            <img src={'hello'} alt={name}/>
          </Col>
          <Col sm="3">
            <img src={'hello'} alt={name}/>
          </Col>
        </Row>

        <Row>
          <Col sm="1">
            <Badge color="primary" pill> Id: </Badge> <label>{id}</label>
          </Col>
          <Col sm="1">
            <Badge color="primary" pill> Name: </Badge> <label>{name}</label>
          </Col>
        </Row>
      </Container>
    
    </>
     );
  }
}
 
export default Content;