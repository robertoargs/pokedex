import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import * as serviceWorker from './serviceWorker';

//Components
import Pokemon from './views/Pokemon/Pokemon';
import Pokedex from './views/Pokedex/Pokedex';
//Styles
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
  <BrowserRouter> 
    <Switch> 
      <Route path="/" exact component={Pokemon}/> 
      <Route path="/pokedex/:id" exact component={Pokedex}/> 
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
