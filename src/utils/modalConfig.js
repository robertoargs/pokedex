export default function modalConfig(order, pkmData) {

  if (order === false) {
    pkmData = {'name':'Charmander',"id":"4", 'sprites':{'front_default':"", "front_shiny":"#", "back_default":"#", "back_shiny":"#"}};
  }
  
  let config = {
    modal:{
      isOpen:order,
      data:pkmData,
      backdrop: true,
      keyboard: true
    }
  }

  return config;
}