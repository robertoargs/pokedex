import React, { Component } from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
  Button
} from 'reactstrap';

import './NavBar.css';

class NavBar extends Component {
  state = { 
    isOpen: false
  }

  render() { 
    return ( 
      <div>
        <Navbar className ="NavBar" light expand="md" fixed="top">
          <NavbarBrand href="/" className="item">Pokemon Application</NavbarBrand>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink href="/pokemon/" className="item">Pokemon</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/berries" className="item">Berries</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/abilities" className="item">Abilities</NavLink>
              </NavItem>
            </Nav>
            <div className="container2">
              <NavbarText className="item">PokeApp List</NavbarText>
            </div>
          <div className="container2">
            <Button color="danger" style={{marginLeft:'1em'}} onClick={this.props.onLess}>Less Pokemon</Button>
            <Button color="success" style={{marginLeft:'1em'}} onClick={this.props.onMore}>More Pokemon</Button>
          </div>
        </Navbar>
      </div>
    );
  }
}
 
export default NavBar;