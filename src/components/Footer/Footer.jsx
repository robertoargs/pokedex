import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
  state = {  }
  render() { 
    return ( 
    <footer className="footer mt-auto py-3 myFooter">
      <div className="container">
        <span className="text-muted">&copy; Pokemon App 2020, All rights reserved</span>
      </div>
    </footer>
     );
  }
}
 
export default Footer;