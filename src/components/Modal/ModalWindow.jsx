import React, { Component } from 'react';
import { 
  Button, 
  Modal, 
  ModalHeader, 
  ModalBody, 
  ModalFooter, 
  Card,
  CardImg,
  CardFooter,
  Col, Row } 
from 'reactstrap';

export default class ModalWindow extends Component {
  render() { 
    const {isOpen} = this.props.open;
    const {id, name} = this.props.open.data;
    const {front_default, front_shiny, back_default, back_shiny} = this.props.open.data.sprites;
    console.log(this.props)
    return ( 
      <div>
        <Modal isOpen={isOpen}>
          <ModalHeader>#{id}: {name}</ModalHeader>
          <ModalBody>
            <Col sm="6">
              <Row> {/* Normal */}
                <Card>
                  <CardImg top src={front_default} alt="hola" className="pkmImage"/>
                  <CardFooter>Normal Front</CardFooter>
                </Card>
                <Card>
                  <CardImg top src ={front_shiny} alt="hola" className="pkmImage"/>
                  <CardFooter>Shiny Front</CardFooter>
                </Card>
              </Row>

              <Row> {/* Shiny */}
                <Card>
                  <CardImg top src={back_default} alt="hola" className="pkmImage"/>
                  <CardFooter>Normal Back</CardFooter>
                </Card>
                <Card>
                  <CardImg top src={back_shiny} alt="hola" className="pkmImage"/>
                  <CardFooter>Shiny Back</CardFooter>
                </Card>
              </Row>
            </Col>
          </ModalBody>

          <ModalFooter>
            <Button color="danger" onClick={this.props.onClose}>Cerrar</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}