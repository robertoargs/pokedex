import React, { Component } from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
  Button
} from 'reactstrap';

import { Link } from 'react-router-dom';

import './NavPokemon.css';

class NavBar extends Component {
  state = { 
    isOpen: false
  }

  render() { 
    return ( 
      <div>
        <Navbar className ="NavBar" light expand="md" fixed="top">
          <NavbarBrand href="/" className="item">Pokemon Application</NavbarBrand>
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink href="/pokemon/" className="item">Pokemon</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/berries" className="item">Berries</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/abilities" className="item">Abilities</NavLink>
              </NavItem>
            </Nav>
            <div className="container2">
              <NavbarText className="item">PokeApp List</NavbarText>
            </div>
          <div className="container2">
            <Link to={`/`}>
              <Button color="info" style={{marginLeft:'1em'}}>Regresar</Button>
            </Link>
          </div>
        </Navbar>
      </div>
    );
  }
}
 
export default NavBar;